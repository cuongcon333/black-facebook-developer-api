javascript: (() => {
	let adAccountID=undefined,
	accessToken = undefined;
	if (window.location.href.match(/asset_id=/))
	{
		adAccountID = window.location.href.match(/asset_id=(.*?)&/)[1];
	}
	if (document.body.innerHTML.match(/init\",\[{\"accessToken\":\"(.*?)\",\"clientID\"/))
	{
		accessToken = document.body.innerHTML.match(/init\",\[{\"accessToken\":\"(.*?)\",\"clientID\"/)[1]
	} else {
		accessToken = window?.__accessToken || require("AdsCMConnectConfig")?.access_token || null;
	}

	
	if (!adAccountID || !accessToken) 
		return alert("Lỗi tài khoản vui lòng kiểm tra lại.");
	
	let PATH = "https://graph.facebook.com/v12.0";
	let fields = { fields: "adspaymentcycle,currency,name,adtrust_dsl,amount_spent", access_token: accessToken };
	let q = new URLSearchParams(fields);

	fetch(`${PATH}/act_${adAccountID}?${q}`, { method: "GET", mode: "cors", credentials: "include" }).then((response) => response.json())
	.then((res) => {
		const { name, adtrust_dsl, amount_spent, currency, id, adspaymentcycle } = res;
		if (id) {
			alert(
				`Account Ads_ID: ${id.replace(/act_/g, "")}\nName Account Ads: ${name}\nLimit: ${adtrust_dsl} ${currency}\nTổng ngưỡng: ${
					adspaymentcycle?.data && adspaymentcycle?.data.length ? `${adspaymentcycle?.data[0].threshold_amount} ${currency}` : `0 ${currency}`
				}\nTổng chi tiêu: ${amount_spent} ${currency}\n\n`
			);
		} else {
			alert("Lỗi, không thể check ngưỡng TK QC này");
		}
	})
	.catch((err) => {
		alert("Lỗi không xác định");
	});
})();
