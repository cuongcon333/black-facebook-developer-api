javascript: (() => {
	let adAccountID = require("BusinessUnifiedNavigationContext").adAccountID || null;

	let accesTokenEG = document.documentElement.innerHTML.match(/"EAAG(.*?)",/);
	let accessToken;
	if (accesTokenEG.length) {
		accessToken = "EAAG" + accesTokenEG[1];
	} else {
		accessToken = require("AdsCMConnectConfig").access_token || null;
	}

	let userID = require("CurrentUserInitialData").ACCOUNT_ID;
	if (!adAccountID || !accessToken || !userID) return alert("Lỗi tài khoản vui lòng kiểm tra lại.");

	let PATH = "https://graph.facebook.com/graphql";

	let val = {
		input: {
			client_mutation_id: 3,
			actor_id: userID,
			logging_data: {
				logging_counter: 21,
				logging_id: "",
			},
			payment_account_id: adAccountID,
		},
	};

	let fields = {
		fb_api_caller_class: "RelayModern",
		fb_api_req_friendly_name: "BillingPrepayUtilsCreateStoredBalanceMutation",
		variables: JSON.stringify(val),
		doc_id: 3138742652811181,
		method: "POST",
		access_token: accessToken,
	};

	let q = new URLSearchParams(fields);

	fetch(`${PATH}?${q}`, { method: "GET", mode: "cors" })
		.then((response) => response.json())
		.then((res) => {
			if (res.errors) {
				alert(res.errors[0].description);
			} else {
				alert("OK");
			}
			console.log(res);
		})
		.catch((err) => {
			alert("Lỗi không xác định");
		});
})();
